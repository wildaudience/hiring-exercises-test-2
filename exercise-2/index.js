const fs = require('fs')
const Stripe = require('stripe');
const STRIPE_TEST_SECRET_KEY = 'rk_test_51EDa6GIrkMUcWfFwXon9vsJYpTqX2eTqbINAUf4fZC7ivToWv59cAPoHdYhmszwL9ZKWJtUUbaCcHtpjl6rWlXLP00C1dcAoUR'
const stripe = Stripe(STRIPE_TEST_SECRET_KEY);
const customers = require('./customers.json');
const countriesCodes = require('./countries-ISO3166.json');

const handler = async (country) => {
  try{

    let finalCustomers = []

    // add here the code
    // filter the customers in country
    const customersInSpain = filterByCountry(country)
  
    // transform customers to save into Stripe
    const customersToSave = transformParams(customersInSpain)
    
    // for each customer create a Stripe customer
    const customersSaved = await saveCustomers(customersToSave)
  
    // push into finalCustomers the stripe customers with email and id as properties.
    finalCustomers = transformSavedCustomers(customersSaved)
    
    // write finalCustomers array into final-customers.json using fs
    const result = await writeIntoFile(finalCustomers)
    console.log(result)

    console.log( finalCustomers )

  }catch(e){
    throw e
  }

} 

const filterByCountry = (country) => {
  return customers.filter(customer=>{
    return customer.country === country
  })
}

const convertCountryCode = (countrieName) => {
  return Object.keys(countriesCodes).find(
    key => countriesCodes[key] === countrieName
    )
}

const transformParams = (customers) => {
  return customers.map(customer=>{
    return {
      email: customer.email,
      address: {
        country: convertCountryCode(customer.country),
        line1: customer.address_line_1,
      },
      name: `${customer.first_name} ${customer.last_name}`
    }
  })
}

const saveCustomers = async (customers) => {
  try{
    const response = customers.map( (customer, i) => {
      return stripe.customers.create(customer);
    })
   return resposne = Promise.all(response)
  }catch(e){
    throw e
  }
}

const transformSavedCustomers = (customers) => {
  return customers.map(customer=>(
    {
      email: customer.email,
      id: customer.id,
      country: customer.address.country
    }
  ))
}

const writeIntoFile = async (data) => {
  return new Promise((res, rej) => {
      fs.writeFile("final-customers.json", JSON.stringify(data), function(err) {
        if(err) {
            rej(err);
        }
        res("The file was saved!");
    }); 
  })
}

const country = 'Spain'
handler(country)