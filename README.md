**Steps**

1. Clone this repository in your local machine

```
https://bitbucket.org/wildaudience/hiring-exercises.git
```

2. Create a new feature branch with your name and surname like [myname-mysurname]

```
git branch myname-mysurname
```

3. Commit as often as you want, and when is done let us know.